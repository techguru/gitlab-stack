### Maintainer: techguru@byiq.com
Copyright (c) 2017,  Cloud Git -- All Rights Reserved

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

# Cloud Git Cluster
A highly scalable and distributed hosting solution for GIT repositories using GitLab CE.
Provides:
1. Certbot/LetsEncrypt automated registration of SSL certificates for public sites
2. Configure small set (11) environment variables to fully deploy a site
3. Site is decomposed as a separate docker service for each server Role used by GitLab

### Public Release August 2017
1. All GitLab containers now `from source`
2. Added Prometheus monitoring container
3. Added Grafana Dashboards
4. Added Google Container Advisor metrics collection
5. Added Docker Registry from official containers.

### Public Release July 2017
1. One-Click deployment after setting small set of configuration variables
2. "A+" rating on https://www.ssllabs.com/ssltest
3. Intended as a development fixture for testing of a decoupled-version of GitLab component services
for behavioral observation and error injection.

### Using of Proof of Concept
1. ensure that you have a working `docker`, `docker-compose`, and `docker-machine`
1. cd `cloud-git-cluster`
2. customize the script `./cgup.sh`
    1. CLOUD_GIT_PROJECT - set to any prefix you like.  Will be prepended to name of docker resources
     (containers, volumes, networks) created by `docker-compose`
    2. CLOUD_GIT_STAGING - when set to the text string `staging`, configures LetsEncrypt via staging
     certificate 
    3. CLOUD_GIT_DOCKER_MACHINE - local or remote machine instance name (from `docker-machine ls`)
3. run the script `./cgup.sh`

### for local development
```
make deploy-master
eval `docker-machine env cloud-git`
```

#### notes
1. Your cluster will receive a self-signed SSL for development purposes if your `docker-machine` is
not reachable from the Internet via DNS lookup matching the Domain Name set in the environment
variable `CLOUD_GIT_DOMAIN` certificate.
2. git+ssh runs by default on port 2222
