#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
listen "cloud-git-gitlab-ce-app:8080", :tcp_nopush => true
listen "/assets/gitlab.socket", :backlog => 1024

working_directory '/var/opt/gitlab/gitlab-rails/working'

timeout 60

preload_app true

worker_processes 3

pid '/opt/gitlab/var/unicorn/unicorn.pid'

stderr_path '/var/log/gitlab/unicorn/unicorn_stderr.log'

stdout_path '/var/log/gitlab/unicorn/unicorn_stdout.log'

ENV['GITLAB_UNICORN_MEMORY_MIN'] = (400 * 1 << 20).to_s

ENV['GITLAB_UNICORN_MEMORY_MAX'] = (650 * 1 << 20).to_s
