#!/bin/sh -
#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

mkdir -p /var/lib/haproxy && mkdir -p /var/log/haproxy
apk add --update curl openssl gettext && rm -rf /var/cache/apk/*
adduser -D -u 1000 haproxy
#
# default is a self-signed certificate for development
##
cd /usr/local/etc/haproxy
envsubst < haproxy.cfg.template > haproxy.cfg
openssl genrsa -out dev-cloud-git-com.key 4096
cp /etc/ssl/openssl.cnf cloud-git-openssl.cnf
openssl req -new -sha256 -key dev-cloud-git-com.key -subj "/C=US/ST=CA/O=Cloud Git, Inc./CN=${CLOUD_GIT_SUBDOMAIN}.${CLOUD_GIT_DOMAIN}" -out dev-cloud-git-com.csr
echo "subjectAltName=DNS:${CLOUD_GIT_SUBDOMAIN}.${CLOUD_GIT_DOMAIN},\
DNS:registry.${CLOUD_GIT_DOMAIN},\
DNS:pages.${CLOUD_GIT_DOMAIN},\
DNS:ci.${CLOUD_GIT_DOMAIN},\
DNS:mattermost.${CLOUD_GIT_DOMAIN},\
DNS:api.${CLOUD_GIT_DOMAIN},\
IP:${CLOUD_GIT_INITIAL_IP}\
 " > extfile.cnf
openssl x509 -req -days 365 -in dev-cloud-git-com.csr -signkey dev-cloud-git-com.key -out dev-cloud-git-com.crt -extfile extfile.cnf
cat dev-cloud-git-com.crt dev-cloud-git-com.key > dev-cloud-git-com.pem
