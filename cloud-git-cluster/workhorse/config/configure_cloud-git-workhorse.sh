#!/bin/sh -
#
# Maintainer: techguru@byiq.com
#
# Copyright (c) 2017,  Cloud Git -- All Rights Reserved
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

set -e # fail on errors

export CLOUD_GIT_GITLAB_WORKHORSE_REPO=https://gitlab.com/gitlab-org/gitlab-workhorse.git
export CLOUD_GIT_GITLAB_WORKHORSE_PACKAGE=gitlab.com/gitlab-org/gitlab-workhorse
export CLOUD_GIT_GITLAB_WORKHORSE_REPO_DIR=/assets/gitlab-workhorse/src/${CLOUD_GIT_GITLAB_WORKHORSE_PACKAGE}

apk add --update curl openssl gettext git && rm -rf /var/cache/apk/*

addgroup -g 998 git
adduser -u 998 -G git -s /bin/sh -h /var/opt/gitlab -D git

mkdir -p ${CLOUD_GIT_GITLAB_WORKHORSE_REPO_DIR}

git clone ${CLOUD_GIT_GITLAB_WORKHORSE_REPO} ${CLOUD_GIT_GITLAB_WORKHORSE_REPO_DIR}
GO15VENDOREXPERIMENT=1 GOPATH=/assets/gitlab-workhorse go install ${CLOUD_GIT_GITLAB_WORKHORSE_PACKAGE}/...

#
# "runas" git user
#
chown -R git:git /assets/gitlab-workhorse/bin
chmod -R g+s,u+s /assets/gitlab-workhorse/bin/*
